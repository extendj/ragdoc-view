import { Parameter } from './parameter';
import { Doc } from './doc';
import { TypeRef } from './type-ref';

export class Group {
  kind: string;
  hidden: boolean;
  members: Member[] = [];

  constructor(kind: string, hidden: boolean, members: Member[]) {
    this.kind = kind;
    this.hidden = hidden;
    this.members = members;
  }

  static fromJson(json: any): Group {
    return new Group(
      json.kind as string,
      (json?.hidden || false) as boolean,
      json.members.map((v: any) => Member.fromJson(v)))
  }
}

export class Member {
  name: string;
  type?: TypeRef;
  doc?: Doc;
  parameters: Parameter[];
  throws: TypeRef[];

  constructor(name: string, type: TypeRef | undefined, doc: Doc | undefined, parameters: Parameter[], throws: TypeRef[]) {
    this.name = name;
    this.type = type;
    this.doc = doc;
    this.parameters = parameters;
    this.throws = throws;
  }

  get isVoid(): boolean {
    return this.type?.isVoid || false;
  }

  static fromJson(json: any): Member {
    var params: Parameter[] = [];
    var doc: Doc | undefined = undefined;
    var name: string = json.name as string;
    if (json.doc) {
      doc = Doc.fromJson(json.doc);
    }
    if (json.params) {
      params = json.params.map((param: any) => Parameter.fromJson(param));
    }
    var throws: TypeRef[] = [];
    if (json.throws) {
      throws = (json.throws as TypeRef[]).map(TypeRef.fromJson);
    }
    if (json.type) {
      return new Member(name, TypeRef.fromJson(json.type), doc, params, throws);
    } else {
      return new Member(name, new TypeRef(name, json.id as string, []), doc, params, throws);
    }
  }
}
