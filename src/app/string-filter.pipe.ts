import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringFilter',
  standalone: true
})
export class StringFilterPipe implements PipeTransform {

  transform(items: any[], filter: string): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item => item.toLowerCase().indexOf(filter.toLowerCase()) >= 0);
  }

}
