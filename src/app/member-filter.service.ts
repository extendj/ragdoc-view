import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class MemberFilterService {
  private filter = new Subject<string>();

  filter$ = this.filter.asObservable();

  constructor() { }

  setFilter(filter: string) {
    this.filter.next(filter);
  }
}