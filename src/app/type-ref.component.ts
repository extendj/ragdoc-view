import { Component, Input, ComponentFactoryResolver } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import {Type} from './type';
import {TypeRef} from './type-ref';
import { MemberFilterService } from './member-filter.service';

@Component({
  selector: 'type-ref',
  standalone: true,
  imports: [CommonModule, RouterModule],
  styles: [`
    .sep {
      display: inline;
      margin-right: .3em;
    }
    .usertype {
      color: #8c1339;
      text-decoration: none;
    }
    .non-usertype {
      color: #444;
    }
  `],
  template: `<ng-container *ngIf="type;else notype"><a *ngIf="type.id; else elseBlock" class="usertype" [routerLink]="['/type', type.id]" (click)="onClick()">{{getName()}}</a><!--
  --><ng-template #elseBlock><span class="non-usertype">{{getName()}}</span></ng-template><!--
    --><ng-container *ngIf="!name && type.args && type.args.length > 0"><!--
      -->&lt;<ng-container *ngFor="let arg of type.args; let isLast=last"><type-ref [type]="arg"></type-ref><div *ngIf="!isLast" class="sep">,</div></ng-container>&gt;<!--
    --></ng-container></ng-container><ng-template #notype>Unknown Type</ng-template>`
})
export class TypeRefComponent {
  @Input() type : TypeRef = new TypeRef('?', '', []);
  @Input() name? : string = undefined;
  @Input() filter : string = "none";

  constructor(private _componentFactoryResolver: ComponentFactoryResolver,
    private memberFilterService: MemberFilterService) { }

  getName(): string {
    if (this.name) {
      return this.name;
    } else {
      return this.type.name;
    }
  }

  onClick() {
    if (this.filter) {
      this.memberFilterService.setFilter(this.filter);
    }
  }
}
