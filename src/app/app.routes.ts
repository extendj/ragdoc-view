import { Routes } from '@angular/router';
import { TypeDetailsComponent } from './type-details.component';
import { SourceViewComponent } from './source-view/source-view.component';

export const routes: Routes = [
  {
    path: 'type/:id',
    component: TypeDetailsComponent
  },
  {
    path: 'source/:filename/:line',
    component: SourceViewComponent
  }
];
