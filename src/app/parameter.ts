import {TypeRef} from './type-ref';

export class Parameter {
  type: TypeRef;
  name: string;

  constructor(type: TypeRef, name: string) {
	  this.type = type;
	  this.name = name;
  }

  static fromJson(json: any): Parameter {
    return {
      type: TypeRef.fromJson(json.t),
      name: json.n,
    };
  }
}

