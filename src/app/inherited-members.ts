import { TypeRef } from './type-ref';

export class InheritedMembers {
  superclass: TypeRef;
  members: string[];

  constructor(superclass: TypeRef, members: string[]) {
    this.superclass = superclass
    this.members = members
  }

  static fromJson(json: any): InheritedMembers {
    return new InheritedMembers(TypeRef.fromJson(json.superclass), json.members as string[]);
  }
}