import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class SelectionService {
  private selection = new Subject<string>();

  selection$ = this.selection.asObservable();

  select(id: string) {
    this.selection.next(id);
  }
}
