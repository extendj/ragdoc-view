import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { Package } from './package';
import { Type } from './type';
import { TypeService } from './type.service';
import { MemberFilterService } from './member-filter.service';
import { SelectionService } from './selection.service';
import { Member } from './member';
import { InheritedMembers } from './inherited-members';
import { Doc } from './doc';
import { Parameter } from './parameter';
import { TypeRefComponent } from './type-ref.component';
import { DeclaredAtComponent } from './declared-at/declared-at.component';
import { ParametersComponent }  from './parameters.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NameFilterPipe } from './name-filter.pipe';
import { StringFilterPipe } from './string-filter.pipe';
import { AstDeclComponent } from './ast-decl/ast-decl.component';

import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'type-details',
  standalone: true,
  styleUrls: ['./type-details.component.css'],
  templateUrl: './type-details.component.html',
  imports: [ CommonModule, FormsModule, TypeRefComponent, DeclaredAtComponent, ParametersComponent, NameFilterPipe, StringFilterPipe, AstDeclComponent ],
  providers: [
    TypeService,
    MemberFilterService,
  ],
})
export class TypeDetailsComponent  implements OnInit {
  type: Type = new Type();
  filter: string = '';

  constructor(private typeService: TypeService,
      private memberFilterService: MemberFilterService,
      private route: ActivatedRoute,
      private location: Location,
      private selectionService: SelectionService) {
    memberFilterService.filter$.subscribe((filter: string) => this.filter = filter);
  }

  ngOnInit() {
    this.route.params.pipe(
	    switchMap((params: Params) => {
		    return this.typeService.getType(params['id']);
	    })
    )
    .subscribe((type: Type) => {
      this.selectionService.select(type.id);
      this.type = type;
    });
  }

  paramDesc(doc: Doc, name: string): string {
    if (doc.params) {
      for (var i = 0; i < doc.params.length; i++) {
        var param = doc.params[i];
        var index = param.indexOf(' ');
        if (index >= 0 && param.substring(0, index) === name) {
          return ' : ' + param.substring(index + 1);
        }
      }
    }
    return '';
  }

  filteredMembers(kind: string): Member[] {
    var group = this.type.groups[kind]
    if (!group) return [];
    var filter = this.filter.toLowerCase();
    var filtered = group.filter((item: Member) => item.name.toLowerCase().indexOf(filter) >= 0)
    return filtered;
  }

  inheritedMembers(inherited: InheritedMembers): boolean {
    var filter = this.filter.toLowerCase();
    var filtered = inherited.members.filter(item => item.toLowerCase().indexOf(filter) >= 0)
    return filtered.length > 0;
  }

  clearFilter() {
    this.filter = '';
  }
}
