import { Component, Input } from '@angular/core';

import {Doc} from '../doc';
import {TypeRef} from '../type-ref';
import {AstDecl} from './ast-decl';
import { TypeRefComponent } from '../type-ref.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ast-decl',
  standalone: true,
  imports: [CommonModule, TypeRefComponent],
  styles: [`
    .ast-decl {
      font-weight: bold;
      padding-left: 3em;
    }
    .ast-component {
      padding-left: 4em;
    }
  `],
  template: `
  <p *ngIf="decl">JastAdd production: <br>
      <div class="ast-decl">{{decl?.name!}}<ng-container *ngIf="decl?.extends!">: <type-ref [type]="decl?.extends!"></type-ref></ng-container><!--
    --><ng-container *ngIf="decl?.components!"> ::= <!--
      --><ng-container *ngFor="let comp of decl?.components!"><!--
        --><div class="ast-component"><!--
        --><ng-container *ngIf="comp.kind == 'regular'"><!--
          --><ng-container *ngIf="comp.name">{{comp.name}}:</ng-container><type-ref [type]="comp.type!"></type-ref> <!--
        --></ng-container><!--
        --><ng-container *ngIf="comp.kind == 'list'"><!--
          --><ng-container *ngIf="comp.name">{{comp.name}}:</ng-container><type-ref [type]="comp.type!"></type-ref>* <!--
        --></ng-container><!--
        --><ng-container *ngIf="comp.kind == 'opt'"><!--
          -->[<ng-container *ngIf="comp.name">{{comp.name}}:</ng-container><type-ref [type]="comp.type!"></type-ref>] <!--
        --></ng-container><!--
        --><ng-container *ngIf="comp.kind == 'token'"><!--
          -->&lt;<ng-container *ngIf="comp.name">{{comp.name}}:</ng-container><type-ref [type]="comp.type!"></type-ref>&gt; <!--
        --></ng-container><!--
        --></div><!--
      --></ng-container><!--
    --></ng-container>
    </div>
  `,
})
export class AstDeclComponent {
  private _decl?: AstDecl;

  constructor() { }

  @Input()
  set decl(decl: AstDecl) {
    this._decl = decl;
  }

  get decl(): AstDecl | undefined {
    return this._decl;
  }
}
