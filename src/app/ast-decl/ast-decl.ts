import {TypeRef} from '../type-ref';
import {AstComponent} from './ast-component';

export class AstDecl {
  name: string = '';
  extends?: TypeRef;
  components: AstComponent[] = [];

  static fromJson(json: any): AstDecl | undefined {
    if (!json) {
      return undefined;
    }
    var ext: TypeRef | undefined = undefined
    if (json.e) {
      ext = TypeRef.fromJson(json.e)
    }
    var components: AstComponent[] = [];
    if (json.c) {
      components = (json.c as any[]).map(c => AstComponent.fromJson(c));
    }
    return {
      name: json.n as string,
      extends: ext,
      components: components,
    };
  }
}
