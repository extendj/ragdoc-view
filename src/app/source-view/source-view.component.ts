import { Component, Input, AfterViewInit, OnInit, ViewChild } from '@angular/core';

import { ControlValueAccessor } from "@angular/forms";
import { ActivatedRoute, Params } from '@angular/router';

import { switchMap } from 'rxjs/operators';

import { SourceService } from '../source.service';
import { FormsModule } from '@angular/forms';

import * as CodeMirror from 'codemirror';

import 'codemirror/mode/clike/clike';
import 'codemirror/addon/selection/active-line';

@Component({
  standalone: true,
  selector: 'app-source-viewer',
  imports: [FormsModule],
  providers: [SourceService],
  template: `<textarea appEditor #editorHolder></textarea>`
})
export class SourceViewComponent
  implements OnInit, AfterViewInit {
  @ViewChild("editorHolder", { static: true })
  editorHolder: any;
  codeMirrorInstance: any;

  constructor(private sourceService: SourceService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      var line = +params['line'];
      this.sourceService.getSource(params['filename']).subscribe((source: string) => {
        var editor = this.codeMirrorInstance;
        editor.setValue(source);
        editor.setCursor({line: line-1, char: 0}, 0);
        editor.scrollIntoView({line: line - 1, char: 0}, 400);
      });
    });
  }

  ngAfterViewInit(): void {
    this.codeMirrorInstance = CodeMirror.fromTextArea(
      this.editorHolder.nativeElement,
      {
        value: '',
        readOnly: true,
        mode: 'text/x-java',
        theme: 'mbo',
        lineNumbers: true,
        styleActiveLine: true,
        lineWrapping: false,
      }
    );
  }
}
