import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable, catchError, retry, throwError, map } from 'rxjs';

import { Package } from './package';

@Injectable()
export class PackageService {
  packageUrl = 'assets/data/packages.json';

  constructor(private http: HttpClient) { }

  getPackages(): Observable<Package[]> {
    return this.http.get(this.packageUrl)
      .pipe(
        retry(3),
        catchError(this.handleError),
        map((res: any) => res.data.map((v: any) => Package.fromJson(v)))
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(`Backend returned code ${error.status}, body was: `, error.error);
    }
    return throwError(() => new Error('Failed to load packages.'))
  }
}
