export class TypeRef {
  name: string = "type-name";
  id: string;
  args: TypeRef[] = [];

  constructor(name: string, id: string, args: TypeRef[]) {
    this.name = name;
    this.id = id;
    this.args = args;
  }

  static fromJson(json: any): TypeRef {
    var args: TypeRef[] = [];
    if (json.a) {
      args = json.a.map((v: any) => TypeRef.fromJson(v));
    }
    if (json.u) {
      // User type.
      if (json.i) {
        return new TypeRef(json.u, TypeRef.typeId(json.u, json.i), args);
      } else {
        return new TypeRef(json.u, TypeRef.simpleName(json.u), args);
      }
    } else {
      // Library or built-in type.
      return new TypeRef(json.n, "undefined-type", args);
    }
  }

  // Remove array and type args.
  static simpleName(name: string): string {
    return TypeRef.strip('<', TypeRef.strip('[', name));
  }

  static strip(tok: string, name: string): string {
    const index = name.indexOf(tok);
    if (index < 0) {
      return name;
    } else {
      var simple = name.substring(0, index);
      return simple;
    }
  }

  get isVoid(): boolean {
    return this.name == 'void';
  }

  static typeId(name: string, idPattern: string): string {
    return idPattern.replace('%', TypeRef.simpleName(name));
  }
}
