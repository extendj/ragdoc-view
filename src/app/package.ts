import { Group } from './member';

export class Package {
  name: string;
  groups: Group[];

  constructor(name: string, groups: Group[]) {
    this.name = name;
    this.groups = groups;
  }

  static fromJson(json: any): Package {
    return new Package(json.name as string, json.groups.map((v: any) => Group.fromJson(v)))
  }
}
