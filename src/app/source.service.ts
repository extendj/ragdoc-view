import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable, catchError, retry, throwError } from 'rxjs';

@Injectable()
export class SourceService {

  constructor(private http: HttpClient) { }

  getSource(filename: string): Observable<any> {
    return this.http.get(`assets/data/${filename}`, { responseType: 'text' })
      .pipe(
	      retry(3),
	      catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
	  if (error.status === 0) {
		  console.error('An error occurred:', error.error);
	  } else {
		  console.error(`Backend returned code ${error.status}, body was: `, error.error);
	  }
	  return throwError(() => new Error('Failed to load source.'))
  }
}
