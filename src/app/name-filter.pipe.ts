import { Pipe, PipeTransform } from '@angular/core';
import { Group, Member } from './member';

@Pipe({
  name: 'nameFilter',
  standalone: true
})

export class NameFilterPipe implements PipeTransform {
  transform(group: Group, filter: string): Member[] {
    if (!filter) {
      return group.members
    }
    return group.members.filter(item => item.name.toLowerCase().indexOf(filter.toLowerCase()) >= 0);
  }
}