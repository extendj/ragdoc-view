import { AstDecl } from './ast-decl/ast-decl';

export class Doc {
  ast: string = '';
  astdecl?: AstDecl;
  ragFile: string = '';
  line: number = 0;
  description: string = '';
  apilevel: string = '';
  params: string[] = [];
  return: string = '';
  attribute: string = '';

  static fromJson(json: any): Doc {
    var astdecl: AstDecl | undefined = undefined;
    if (json.astdecl) {
      astdecl = AstDecl.fromJson(json.astdecl);
    }
    return {
      ast: json.ast,
      astdecl: astdecl,
      ragFile: json.ragFile,
      line: json.line,
      description: json.description,
      apilevel: json.apilevel,
      params: json.params,
      return: json.return,
      attribute: json.attribute,
    };
  }
}
