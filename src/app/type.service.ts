import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, catchError, retry, throwError, map } from 'rxjs';

import { Type } from './type';

@Injectable()
export class TypeService {

  constructor(private http: HttpClient) { }

  getType(id: string): Observable<Type> {
    return this.http.get<Type>(`assets/data/${id}.json`)
      .pipe(
        retry(3),
        catchError(error => {
          if (error.status === 0) {
            console.error('An error occurred:', error.error);
          } else {
            console.error(`Backend returned code ${error.status}, body was: `, error.error);
          }
          return throwError(() => new Error(`Failed to load type ${id}.`))
        }),
        map((res: any) => Type.fromJson(res.data))
      );
  }
}
