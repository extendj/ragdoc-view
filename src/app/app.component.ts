import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { Package } from './package';
import { Type } from './type';
import { PackageService } from './package.service';
import { SelectionService } from './selection.service';
import { Parameter } from './parameter';
import { NameFilterPipe } from './name-filter.pipe';
import { RouterModule } from '@angular/router';
import { Group, Member } from './member';
import { HttpClientModule } from '@angular/common/http';

import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  styleUrls: ['./app.component.css'],
  templateUrl: './app.component.html',
  imports: [CommonModule, FormsModule, NameFilterPipe, RouterModule, HttpClientModule ],
  providers: [
    PackageService,
    SelectionService,
  ],
})
export class AppComponent implements OnInit {
  title = 'ExtendJ API Documentation';
  showMenu = false;
  packages: Package[] = [];
  filter: string = '';
  selectedType = '';

  typeKindNames: { [id: string]: string } = {
    'ast-class': 'AST CLASSES',
    'interface': 'INTERFACES',
    'class': 'CLASSES',
  };

  constructor(private packageService: PackageService, private selectionService: SelectionService) {
    selectionService.selection$.subscribe((id: string) => this.selectedType = id);
  }

  ngOnInit(): void {
    this.packageService.getPackages().subscribe(packages => this.packages = packages);
  }

  filteredPackage(pkg: Package): boolean {
    var filter = this.filter.toLowerCase();
    for (var i = 0; i < pkg.groups.length; i++) {
      if (this.filteredGroup(pkg.groups[i])) {
        return true;
      }
    }
    return false;
  }

  filteredGroup(group: Group): boolean {
    var filter = this.filter.toLowerCase();
    var filtered = group.members.filter(member => member.name.toLowerCase().indexOf(filter) >= 0);
    return filtered.length > 0;
  }
}

