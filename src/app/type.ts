import {Doc} from './doc';
import {Group, Member} from './member';
import {TypeRef} from './type-ref';
import {InheritedMembers} from './inherited-members';

export interface Groups {
  [id: string]: Member[];
}

export class Type {
  id: string = "type";
  kind: string = "kind";
  name: string = "name";
  pkg: string = "";
  mods: string[] = [];
  doc?: Doc;
  groups: Groups = {};
  args: TypeRef[] = []; // Type arguments.
  superclass?: TypeRef;
  superinterfaces: TypeRef[] = [];
  inherited_methods: InheritedMembers[] = [];
  inherited_attributes: InheritedMembers[] = [];
  inherited_fields: InheritedMembers[] = [];
  subtypes: TypeRef[] = [];

  static fromJson(json: any): Type {
    var doc: Doc | undefined = undefined;
    if (json.doc) {
      doc = Doc.fromJson(json.doc);
    }
    var groups: Groups = {};
    if (json.groups) {
      for (var i = 0; i < json.groups.length; i++) {
        var group = json.groups[i];
        if (group.members) {
          groups[group.kind as string] = group.members.map((member: any) => Member.fromJson(member));
        }
      }
    }
    var superclass: TypeRef | undefined = undefined;
    if (json.superclass) {
      superclass = TypeRef.fromJson(json.superclass);
    }
    var superinterfaces: TypeRef[] = [];
    if (json.superinterfaces) {
      superinterfaces = (json.superinterfaces as TypeRef[]).map(TypeRef.fromJson);
    }
    var inherited_methods: InheritedMembers[] = [];
    if (json.inherited_methods) {
      inherited_methods = (json.inherited_methods as any[]).map(inherited =>
          InheritedMembers.fromJson(inherited));
    }
    var inherited_attributes: InheritedMembers[] = [];
    if (json.inherited_attributes) {
      inherited_attributes = (json.inherited_attributes as any[]).map(inherited =>
          InheritedMembers.fromJson(inherited));
    }
    var inherited_fields: InheritedMembers[] = [];
    if (json.inherited_fields) {
      inherited_fields = (json.inherited_fields as any[]).map(inherited =>
          InheritedMembers.fromJson(inherited));
    }
    var args: TypeRef[] = [];
    if (json.args) {
      args = (json.args as any[]).map(arg => TypeRef.fromJson(arg));
    }
    var subtypes: TypeRef[] = [];
    if (json.subtypes) {
      subtypes = (json.subtypes as any[]).map(arg => TypeRef.fromJson(arg));
    }
    return {
      id: TypeRef.typeId(json.name, json.id),
      kind: json.kind,
      name: json.name,
      pkg: json.pkg,
      mods: json.mods as string[],
      doc: doc,
      groups: groups,
      args: args,
      superclass: superclass,
      superinterfaces: superinterfaces,
      inherited_methods: inherited_methods,
      inherited_attributes: inherited_attributes,
      inherited_fields: inherited_fields,
      subtypes: subtypes,
    };
  }
}

