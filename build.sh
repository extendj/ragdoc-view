#!/bin/bash

set -eu

EXTENDJ="../extendj"
RD_BUILDER="../ragdoc-builder/rd-builder.jar"

# Optional debug flags.
DEBUG="${DEBUG:-}"
#DEBUG="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005"

git clean -fx src/assets/data
java \
  $DEBUG \
  -jar "${RD_BUILDER}" \
  -d src/assets/data \
  -ragroot $EXTENDJ \
  $(find $EXTENDJ/src -name '*.java') \
  $(find $EXTENDJ/java8/src -name '*.java')

# Build packed assets for publishing.
ng build -c production --base-href "/doc3/"
